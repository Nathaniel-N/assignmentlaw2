from django.db import models

# Create your models here.
class Card(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    converted_mana_cost = models.IntegerField()
