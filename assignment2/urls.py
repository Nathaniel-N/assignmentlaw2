"""assignment2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from django.views.generic.base import RedirectView

import auth_token.urls as auth_token
import compress.urls as compress
import files.urls as files
import metadata.urls as metadata

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^auth_token/', include(auth_token, namespace='auth_token')),
    url(r'^compress/', include(compress, namespace='compress')),
    url(r'^files/', include(files, namespace='files')),
    url(r'^metadata/', include(metadata, namespace='metadata')),
    url(r'^$', RedirectView.as_view(url='/auth_token/', permanent = 'true'), name='auth_token'),
]
