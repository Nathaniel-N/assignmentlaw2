from django.forms import ModelForm
from django import forms
from .models import File

class FileForm(ModelForm):
    class Meta:
        model = File
        fields = ('name', 'file')
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control'
                    }
                ),
            'file': forms.FileInput(
                attrs={
                    'class': 'form-control'
                    } 
                )
            }
