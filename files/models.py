from django.db import models

# Create your models here.
class File(models.Model):
    name = models.CharField(max_length=512, blank=False)
    file = models.FileField(upload_to='files/')
