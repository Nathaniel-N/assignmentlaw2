from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from rest_framework import generics
from rest_framework.response import Response

from .models import Card
from .serializers import CardSerializer
from .permissions import IsAuthenticated

class CardList(generics.ListCreateAPIView):
    queryset = Card.objects.all()
    serializer_class = CardSerializer
    permission_classes = [IsAuthenticated]

class CardDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Card.objects.all()
    serializer_class = CardSerializer
    permission_classes = [IsAuthenticated]

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

