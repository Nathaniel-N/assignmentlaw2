from django.conf.urls import url
from .views import get_token
#url for app
app_name = 'auth_token'

urlpatterns = [
    url(r'^$', get_token, name='get_token'),
]
