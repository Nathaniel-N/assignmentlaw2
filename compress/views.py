from django.shortcuts import render, redirect
import requests

# Create your views here.
def index(request):
    response = {'authorized': False, 'value': 'Authorization required'}
    if 'access_token' in request.session:
        access_token = request.session['access_token']
        URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/resource"
        data_headers = { 'Authorization': 'Bearer ' + access_token }
        r = requests.get(url = URL, headers = data_headers).json()
        if 'error' in r:
            response['value'] = request.session['error_description']
        else:
            response['authorized'] = True
            response['value'] = 'Authorization successful but microservice not implemented yet'

    return render(request, "compress.html", response)
