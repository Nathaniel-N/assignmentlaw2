from django.conf.urls import url
from .views import index
#url for app
app_name = 'compress'

urlpatterns = [
    url(r'^$', index, name='index'),
]
