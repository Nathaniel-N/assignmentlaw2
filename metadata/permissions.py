from rest_framework import permissions
import requests

class IsAuthenticated(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if 'access_token' in request.session:
            access_token = request.session['access_token']
            URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/resource"
            data_headers = { 'Authorization': 'Bearer ' + access_token }
            r = requests.get(url = URL, headers = data_headers).json()
            if 'error' in r:
                return False
            else:
                return True
        return False
