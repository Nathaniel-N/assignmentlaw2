from django.shortcuts import render, redirect
from .forms import FileForm
from .models import File
import requests

# Create your views here.
def index(request):
    response = {'authorized': False, 'value': 'Authorization required'}
    if 'access_token' in request.session:
        access_token = request.session['access_token']
        URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/resource"
        data_headers = { 'Authorization': 'Bearer ' + access_token }
        r = requests.get(url = URL, headers = data_headers).json()
        if 'error' in r:
            response['value'] = request.session['error_description']
        else:
            response['authorized'] = True
            response['value'] = 'Authorization successful'
            response['files'] = File.objects.all()
            if request.method == 'POST':
                form = FileForm(request.POST, request.FILES)
                if form.is_valid():
                    form.save()
                    return redirect('/files')
            else:
                form = FileForm()
            response['form'] = form


    return render(request, "files.html", response)
