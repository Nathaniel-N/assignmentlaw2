from django.conf.urls import url
from .views import CardList, CardDetail
#url for app
app_name = 'metadata'

urlpatterns = [
    url(r'^$',
        CardList.as_view(),
        name="card-list"),
    url(r'^(?P<pk>\d+)/$',
        CardDetail.as_view(),
        name="card-detail"),
]
